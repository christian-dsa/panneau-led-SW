import minimalmodbus
import time

address = 10
instrument = minimalmodbus.Instrument('/dev/ttyUSB0', address, minimalmodbus.MODE_RTU)
instrument.serial.baudrate = 19200
instrument.serial.parity = minimalmodbus.serial.PARITY_EVEN
instrument.serial.stopbits = minimalmodbus.serial.STOPBITS_ONE
instrument.serial.bytesize = minimalmodbus.serial.EIGHTBITS
instrument.serial.timeout = 0.05

print(instrument)

while 1:

    try:
        instrument.write_register(1002, 25, 0, 6)

        holding_register = int(instrument.read_register(0))  # Register number, number of decimals
        print("Register 0 = " + hex(holding_register))
        holding_register = int(instrument.read_register(1))  # Register number, number of decimals
        print("Register 1 = " + hex(holding_register))
        holding_register = int(instrument.read_register(2))  # Register number, number of decimals
        print("Register 2 = " + hex(holding_register))
        holding_register = int(instrument.read_register(3))  # Register number, number of decimals
        print("Register 3 = " + hex(holding_register))
        holding_register = int(instrument.read_register(4))  # Register number, number of decimals
        print("Register 4 = " + hex(holding_register))
        holding_register = int(instrument.read_register(5))  # Register number, number of decimals
        print("Register 5 = " + hex(holding_register))

        # set new address
        # instrument.write_register(2, 10, 0, 6)     # set address
        # instrument.write_register(6, 0xD742, 0, 6) # Reset
    except minimalmodbus.NoResponseError:
        pass

    time.sleep(1)

    exit()
