Feature: Control light intensity
    
    Light intensity can be controlled via MODBUS (manual mode) or 
    via the programmed schedule (schedule mode).

    In manual mode, write the desired intensity in % at the desired
    channel. Reading the channel gives the current intensity.

    Scenario: Light intensity is manually controlled via MODBUS
        Given Device has serial configuration Baudrate: 115200 baud, parity: Even, Stop bit: 1 and Address: 247
        And   is set to manual mode
        When  channel 1 is set to 25%
        Then  the register read 25

        Given Device has serial configuration Baudrate: 115200 baud, parity: Even, Stop bit: 1 and Address: 247
        And   is set to manual mode
        When  channel 1 is set to 125%
        Then  the register read 100

        Given Device has serial configuration Baudrate: 115200 baud, parity: Even, Stop bit: 1 and Address: 247
        And   is set to manual mode
        When  all channels are set to 50%
        Then  all channel registers read 50

        Given Device has serial configuration Baudrate: 115200 baud, parity: Even, Stop bit: 1 and Address: 247
        And   is set to manual mode
        When  Device is reset
        Then  all channels are OFF and read 0
