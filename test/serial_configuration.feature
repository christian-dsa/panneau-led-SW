Feature: Configuration

    Default configuration:
    Address : 247
    Baudrate: 19200 baud
    Parity  : E (Even)
    Stop bit: 1

    Scenario Outline: Change device's baudrate configuration
        Given Device has serial configuration Baudrate: 19200 baud, parity: E, Stop bit: 1 and Address: 247
        When  baudrate is changed to <baudrate> baud
        And   device is reset, wait 1.5 seconds
        Then  new baudrate is <baudrate> baud

        Examples:
        | baudrate |
#        |    300   | Not supported on REV100
#        |    600   | Not supported on REV100
        |   1200   |
        |   2400   |
        |   4800   |
        |   9600   |
        |  19200   |
        |  38400   |
        |  57600   |
        | 115200   |

    Scenario Outline: Change device's parity configuration
        Given Device has serial configuration Baudrate: 19200 baud, parity: E, Stop bit: 1 and Address: 247
        When  parity is changed to <parity>
        And   device is reset, wait 1.5 seconds
        Then  new parity is <parity>

        Examples:
        | parity |
        |   N    |
        |   O    |
        |   E    |

    Scenario Outline: Change device's stop bit configuration
        Given Device has serial configuration Baudrate: 19200 baud, parity: E, Stop bit: 1 and Address: 247
        When  stop bit is changed to <stop>
        And   device is reset, wait 1.5 seconds
        Then  new stop config is <stop>

        Examples:
        | stop |
        |  1   |
        |  2   |

    Scenario Outline: Change device address
        Given Device has serial configuration Baudrate: 19200 baud, parity: E, Stop bit: 1 and Address: 247
        When  device address is changed to value <address>
        And   device is reset, wait 1.5 seconds
        Then  it answers to address <address>

        Examples:
        | address |
        |   128   |
        |   247   |
