from pytest_bdd import scenario, given, when, then, parsers
from src.light_modbus import LightModbus


@scenario('rtc_calibration.feature', "Calculate register value if error value within -488.5 ppm to +487.1 ppm "
                                     "with a 512 Hz calibration clock.",
          example_converters=dict(error=float))
def test_rtc_calibration():
    pass


@given("The measured calibration clock is off by <error> ppm", target_fixture="get_rtc")
def get_rtc(error):
    calibration_freq = 512
    measured_freq = (1 + float(error) / 1e6) * calibration_freq

    return dict(clock_error_ppm=error,
                calibration_value=0,
                calibration_freq_hz=calibration_freq,
                measured_freq_hz=measured_freq)


@when("calibration value is calculated")
def calc_calibration(get_rtc):
    # noinspection PyProtectedMember
    calibration_val = LightModbus._rtc_calibration_cal(get_rtc['calibration_freq_hz'], get_rtc['measured_freq_hz'])
    get_rtc['calibration_value'] = calibration_val


@then(parsers.parse("the final error is <= {max_error:f} ppm"))
def assert_calibrated_clock(get_rtc, max_error):
    # noinspection PyProtectedMember
    error = 1 - get_rtc['calibration_freq_hz'] / LightModbus._calibrated_clock(get_rtc['measured_freq_hz'],
                                                                               get_rtc['calibration_value'])
    error_ppm = abs(error) * 1e6
    assert error_ppm <= max_error, "Error is too big!"
