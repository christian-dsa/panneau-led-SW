from pytest_bdd import scenario, given, when, then


@then("Firmware version is 0.2.0 & Board version is REV100")
def step_impl():
    raise NotImplementedError(u'STEP: Then  Firmware version is 0.2.0 & Board version is REV100')
