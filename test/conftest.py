from collections import deque
from pytest_bdd import given, parsers
from src.light_modbus import LightModbus
import pytest
import time
import logging

"""
 Device address used in tests
"""
address_range = [247, 128]

"""
 Modbus timeout (in seconds)
"""
timeout = 1

"""
 Reset delay before the device comes back
"""
reset_delay = 2


class Dut:
    def __init__(self, port):
        self.serial_config = {'Address': None, 'Baudrate': None, 'Parity': None, 'Stop bit': None}
        self.timeout = timeout
        try:
            self.device = LightModbus(port)
        except Exception:
            error_message = "Device under test not connected"
            logging.error(error_message)
            pytest.exit(error_message)

    def find_serial_config(self, expected_config=None):
        """ Find device's current serial configuration

        It starts from the expected configuration. If the expected configuration is not working, it search within
        all valid values. Since there are a lot of valid address, it is possible to pass a list. If 'Address' is None,
        it search in the whole range.

        :param expected_config: dict{'Address', 'Baudrate': None, 'Parity': None, 'Stop bit': None}
        :return:
        """
        if expected_config['Address'] is None:
            expected_config['Address'] = self.device.valid_address

        if expected_config['Baudrate'] is None:
            expected_config['Baudrate'] = self.device.valid_baudrate
        else:
            baudrate = deque(self.device.valid_baudrate)
            baudrate.rotate(-1 * baudrate.index(expected_config['Baudrate']))
            expected_config['Baudrate'] = list(baudrate)

        if expected_config['Parity'] is None:
            expected_config['Parity'] = self.device.valid_parity
        else:
            parity = deque(self.device.valid_parity)
            parity.rotate(-1 * parity.index(expected_config['Parity']))
            expected_config['Parity'] = list(parity)

        if expected_config['Stop bit'] is None:
            expected_config['Stop bit'] = self.device.valid_stop_bit
        else:
            stop_bit = deque(self.device.valid_stop_bit)
            stop_bit.rotate(-1 * stop_bit.index(expected_config['Stop bit']))
            expected_config['Stop bit'] = list(stop_bit)

        self.serial_config = self.device.find_device_serial_config(expected_config['Address'],
                                                                   expected_config['Baudrate'],
                                                                   expected_config['Parity'],
                                                                   expected_config['Stop bit'],
                                                                   timeout)

        if self.serial_config['Address'] is None:
            error_message = "Serial configuration not found, can't communicate with device. Exit"
            logging.error(error_message)
            pytest.exit(error_message)
        return self.serial_config


def assert_input_parity(parity=None):
    """ Convert parity value in Gherkins to Device parity value 
    
    :param parity: Valid value: 'E'(Even), 'O'(Odd) & 'N'(None)
    """
    if parity in ['E', 'O', 'N']:
        pass
    else:
        pytest.exit("Parity in Gherkins must be one of these: 'E'(Even), 'O'(Odd) & 'N'(None)")


def pytest_addoption(parser):
    parser.addoption("--port", action="store", default="/dev/ttyUSB0")


@pytest.fixture(scope="session")
def dut(pytestconfig):
    port = pytestconfig.getoption("port")
    return Dut(port)


@given(parsers.parse(
    "Device has serial configuration Baudrate: {setup_baudrate:d} baud, parity: {setup_parity},"
    " Stop bit: {setup_stop_bit:d} and Address: {setup_address:d}"),
    target_fixture="setup_device")
def setup_device(dut, setup_baudrate, setup_parity, setup_stop_bit, setup_address):
    logging.info(f"Setting up device to Baudrate: {setup_baudrate}, Parity: {setup_parity}, Stop bit: {setup_stop_bit} "
                 "and Address: {setup_address}")

    assert_input_parity(setup_parity)

    dut.find_serial_config({'Address': address_range,
                            'Baudrate': dut.serial_config['Baudrate'],
                            'Parity': dut.serial_config['Parity'],
                            'Stop bit': dut.serial_config['Stop bit']})

    need_reset = False
    if dut.serial_config['Address'] != setup_address:
        dut.device.set_address(setup_address)
        need_reset = True

    if dut.serial_config['Baudrate'] != setup_baudrate:
        dut.device.set_baudrate(setup_baudrate)
        need_reset = True
        pass

    if dut.serial_config['Parity'] != setup_parity:
        dut.device.set_parity(setup_parity)
        need_reset = True
        pass

    if dut.serial_config['Stop bit'] != setup_stop_bit:
        dut.device.set_stop_bit(setup_stop_bit)
        need_reset = True
        pass

    if need_reset is True:
        dut.device.reset()
        dut.device.serial_close()

        time.sleep(reset_delay)

        dut.device.serial_open()
        dut.serial_config = dut.device.serial_config(setup_address,
                                                     setup_baudrate,
                                                     setup_parity,
                                                     setup_stop_bit,
                                                     timeout)

        if dut.device.is_device_responding() is False:
            error_message = "Device not responding, can't continue"
            logging.error(error_message)
            pytest.exit(error_message)
