# Created by christian at 2021-09-09

Feature: Time configuration
  Configure the device's time

    Scenario Outline: Configure and read time
        Given Device has serial configuration Baudrate: 19200 baud, parity: E, Stop bit: 1 and Address: 247
        When  time is configured to <week_day> <start>
        When  wait <delay> seconds
        Then  board's time is <end> and <week_day>

        Examples:
        | start    | delay | end      | week_day |
        | 16:30:10 | 5     | 16:30:15 | Thursday |

    Scenario Outline: Configure calibration value
        Given Device has serial configuration Baudrate: 19200 baud, parity: E, Stop bit: 1 and Address: 247
        When  RTC's calibration is set to <value>
        Then  device RTC's calibration is <value>

        Examples:
        | value |
        | 0     |
        | 316   |
