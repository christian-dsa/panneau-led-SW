Feature: Firmware version
    Validate the target has the right firmware and the right board
    
    Firmware: Test on the repo are always for the latest version. 
              To test a previous released version, checkout its tag.
    Board: There is currently only one board and one revision. REV100

    Scenario: Version
        Given Device has serial configuration Baudrate: 115200 baud, parity: Even, Stop bit: 1 and Address: 247
        Then  Firmware version is 0.2.0 & Board version is REV100
