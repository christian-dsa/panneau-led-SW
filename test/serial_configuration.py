from pytest_bdd import scenario, when, then, parsers
import logging
import time


@scenario('serial_configuration.feature', "Change device's baudrate configuration",
          example_converters=dict(baudrate=int))
def test_baudrate_config():
    pass


@scenario('serial_configuration.feature', "Change device's parity configuration",
          example_converters=dict(parity=str))
def test_parity_config():
    pass


@scenario('serial_configuration.feature', "Change device's stop bit configuration",
          example_converters=dict(stop=int))
def test_stop_config():
    pass


@scenario('serial_configuration.feature', "Change device address",
          example_converters=dict(address=int))
def test_address_config():
    pass


@when("baudrate is changed to <baudrate> baud")
def set_new_baudrate(dut, baudrate):
    logging.info(f"Change baudrate to: {baudrate}")
    dut.device.set_baudrate(baudrate)


@when(parsers.parse("device is reset, wait {delay_seconds} seconds"))
def reset_device(dut, delay_seconds):
    # Get delay_seconds as a string and then convert to float since it is not possible to pass it directly as a float
    delay_seconds = float(delay_seconds)

    logging.info(f"Reset device, wait {delay_seconds:.1f} seconds")
    dut.device.reset()
    dut.device.serial_close()
    time.sleep(delay_seconds)
    dut.device.serial_open()


@then("new baudrate is <baudrate> baud")
def assert_baudrate(dut, baudrate):
    logging.info(f"Assert Baudrate: {baudrate}")

    serial_config = dut.device.serial_config(dut.serial_config['Address'],
                                             baudrate,
                                             dut.serial_config['Parity'],
                                             dut.serial_config['Stop bit'],
                                             dut.timeout)

    assert dut.device.is_device_responding(), "Device not responding"
    assert dut.device.get_baudrate() == int(baudrate), "Register value is wrong"

    # All assert passed. Serial config is good
    dut.serial_config = serial_config


@when("parity is changed to <parity>")
def set_parity(dut, parity):
    logging.info(f"Change parity to: {parity}")
    dut.device.set_parity(parity)


@then("new parity is <parity>")
def assert_parity(dut, parity):
    logging.info(f"Assert Parity: {parity}")
    serial_config = dut.device.serial_config(dut.serial_config['Address'],
                                             dut.serial_config['Baudrate'],
                                             parity,
                                             dut.serial_config['Stop bit'],
                                             dut.timeout)

    assert dut.device.is_device_responding(), "Device not responding"
    assert dut.device.get_parity() == parity, "Register value is wrong"

    # All assert passed. Serial config is good
    dut.serial_config = serial_config


@when("stop bit is changed to <stop>")
def set_stop(dut, stop):
    logging.info(f"Change parity to: {stop}")
    dut.device.set_stop_bit(stop)


@then("new stop config is <stop>")
def assert_stop(dut, stop):
    logging.info(f"Assert Stop: {stop}")
    serial_config = dut.device.serial_config(dut.serial_config['Address'],
                                             dut.serial_config['Baudrate'],
                                             dut.serial_config['Parity'],
                                             stop,
                                             dut.timeout)

    assert dut.device.is_device_responding(), "Device not responding"
    assert dut.device.get_stop_bit() == stop, "Register value is wrong"

    # All assert passed. Serial config is good
    dut.serial_config = serial_config


@when("device address is changed to value <address>")
def set_device_address(dut, address):
    logging.info(f"Change address to: {address}")
    dut.device.set_address(address)


@then("it answers to address <address>")
def assert_address(dut, address):
    logging.info(f"Assert Address: {address}")
    serial_config = dut.device.serial_config(address,
                                             dut.serial_config['Baudrate'],
                                             dut.serial_config['Parity'],
                                             dut.serial_config['Stop bit'],
                                             dut.timeout)

    assert dut.device.is_device_responding(), "Device not responding"
    assert dut.device.get_address() == address, "Register value is wrong"

    # All assert passed. Serial config is good
    dut.serial_config = serial_config
