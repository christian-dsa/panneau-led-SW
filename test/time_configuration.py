import time

from pytest_bdd import scenario, when, then


@scenario("time_configuration.feature", "Configure and read time",
          example_converters=dict(start=str, delay=float, end=str, week_day=str))
def test_time_config():
    pass


@scenario("time_configuration.feature", "Configure calibration value",
          example_converters=dict(value=int))
def test_time_calibration():
    pass


@when("time is configured to <week_day> <start>")
def set_time(dut, week_day, start):
    time_str = f"{week_day} {start}"
    t = time.strptime(time_str, "%A %H:%M:%S")
    dut.device.set_time(t)


@when("wait <delay> seconds")
def wait(delay):
    time.sleep(float(delay))


@then("board's time is <end> and <week_day>")
def assert_time(dut, end, week_day):
    expected_time_str = f"{week_day} {end}"

    t = dut.device.get_time()
    t_str = time.strftime("%A %H:%M:%S", t)

    assert t_str == expected_time_str, "Time read and expected time don't match!"


@when("RTC's calibration is set to <value>")
def set_rtc_calibration(dut, value):
    dut.device.set_rtc_calibration_value(value)


@then("device RTC's calibration is <value>")
def assert_rtc_calibration_value(dut, value):
    calibration_value = dut.device.get_rtc_calibration_value()

    assert calibration_value == value, "Calibration value was not saved"
