# Created by christian at 2021-09-08

Feature: RTC Calibration
  Calibrate the board's Real Time Clock by measuring the 1Hz calibration clock.

    Scenario Outline: Calculate register value if error value within -488.5 ppm to +487.1 ppm with a 512 Hz calibration clock.
        Given The measured calibration clock is off by <error> ppm
        When  calibration value is calculated
        Then  the final error is <= 0.954 ppm

        Examples:
        | error  |
        | -488.5 |
        | -200.0 |
        | -100.8 |
        | -88.2  |
        | -24.3  |
        | -11.1  |
        | -5.8   |
        | -1.2   |
        | -0.955 |
        |  0.955 |
        |  1.8   |
        |  4.0   |
        |  9.54  |
        |  12.5  |
        |  26.2  |
        |  76.9  |
        |  132.1 |
        |  264.0 |
        |  487.1 |
