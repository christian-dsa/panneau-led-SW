import minimalmodbus
import numpy as np
import time


class LightModbus:

    def __init__(self, port):
        """

        :param port: Computer port connected to RS-485 adapter
        """

        " Baudrate lookup table. Key is the baudrate, value is light modbus register value "
        self._baudrate_lookup = {300: 0, 600: 1, 1200: 2, 2400: 3, 4800: 4, 9600: 5, 19200: 6, 38400: 7, 57600: 8,
                                 115200: 9}
        self._parity_lookup = {'N': 0, 'O': 1, 'E': 2}

        " Serial Modbus config "
        self.port = port
        self.mode = minimalmodbus.MODE_RTU
        try:
            self._instrument = minimalmodbus.Instrument(self.port, None)
        except Exception:
            raise ValueError("Port not valid")

        self.valid_address = range(1, 248)
        self.valid_baudrate = list(self._baudrate_lookup.keys())
        self.valid_parity = list(self._parity_lookup.keys())
        self.valid_stop_bit = [1, 2]

        " Reset "
        self._reset_key = 0xD742

        " RTC Calibration "
        self._rtc_calibration_freq = 512

        " Device Registers "
        self._registers = {
            "FW version": 0,
            "Board version": 1,
            "Serial Address": 2,
            "Serial Baud Rate": 3,
            "Serial Parity": 4,
            "Serial Stop Bit": 5,
            "Reset": 6,
            "RTC Second": 7,
            "RTC Minute": 8,
            "RTC Hour": 9,
            "RTC Day of week": 10,
            "RTC Calibration": 11,
            "Light mode": 100,
            "Light intensity ALL": 1000,
            "Light intensity CH1": 1001,
            "Light intensity CH2": 1002,
            "Light intensity CH3": 1003,
            "Light intensity CH4": 1004,
            "Light intensity CH5": 1005,
        }

        " Device Coils "
        self._coils = {
            "Light channel 1 status": 1,
            "Light channel 2 status": 2,
            "Light channel 3 status": 3,
            "Light channel 4 status": 4,
            "Light channel 5 status": 5,
        }

    def serial_open(self):
        """ Open serial port
        """
        self._instrument.serial.open()

    def serial_close(self):
        """ Close serial port
        """
        self._instrument.serial.close()

    def serial_config(self, address, baudrate, parity, stop_bit, timeout):
        """ Configure client's serial modbus

        Invalid parameters will default to:
        address: 247
        baudrate: 19200
        parity: Even
        stop_bit: 1

        :param address: 1 to 247
        :param baudrate: valid values are [300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200]
        :param parity: 'E' (Even), 'O' (Odd) or 'N' (None)
        :param stop_bit: 1 or 2
        :param timeout: timeout value in seconds
        :return Dict with all parameters value
        """
        if address in self.valid_address:
            self._instrument.address = address
        else:
            self._instrument.address = 247

        if baudrate in self.valid_baudrate:
            self._instrument.serial.baudrate = baudrate
        else:
            self._instrument.serial.baudrate = 19200

        if parity in self.valid_parity:
            self._instrument.serial.parity = parity
        else:
            self._instrument.serial.parity = 'E'

        if stop_bit in self.valid_stop_bit:
            self._instrument.serial.stop_bit = stop_bit
        else:
            self._instrument.serial.stop_bit = 1

        self._instrument.serial.timeout = timeout

        return {'Address': self._instrument.address, 'Baudrate': self._instrument.serial.baudrate,
                'Parity': self._instrument.serial.parity, 'Stop bit': self._instrument.serial.stop_bit,
                'Timeout': self._instrument.serial.timeout}

    def find_device_serial_config(self,
                                  address_range=None,
                                  baudrate_range=None,
                                  parity_range=None,
                                  stop_bit_range=None,
                                  timeout=1):
        """ Find device serial modbus configuration

        :param address_range: possible address, must be iterable. If None, will use all valid address as range.
        :param baudrate_range: possible baudrate, must be iterable. If None, will use all valid baudrate as range.
        :param parity_range: possible parity, must be iterable. If None, will use all valid parity as range.
        :param stop_bit_range: possible stop bit, must be iterable. If None, will use all valid stop bit as range.
        :param timeout: timeout in seconds
        :return: Dict with all parameters value if found. Parameters = None when not found.
        """

        if address_range is None:
            address_range = self.valid_address

        if baudrate_range is None:
            baudrate_range = self.valid_baudrate

        if parity_range is None:
            parity_range = self.valid_parity

        if stop_bit_range is None:
            stop_bit_range = self.valid_stop_bit

        for address in address_range:
            for baudrate in baudrate_range:
                for parity in parity_range:
                    for stop_bit in stop_bit_range:
                        self.serial_config(address, baudrate, parity, stop_bit, timeout)

                        if self.is_device_responding():
                            return {'Address': address, 'Baudrate': baudrate, 'Parity': parity, 'Stop bit': stop_bit}

        return {'Address': None, 'Baudrate': None, 'Parity': None, 'Stop bit': None}

    def get_modbus_config(self):
        return {'Address': self._instrument.address, 'Baudrate': self._instrument.serial.baudrate,
                'Parity': self._instrument.serial.parity, 'Stop bit': self._instrument.serial.stop_bit,
                'Timeout': self._instrument.serial.timeout}

    def set_address(self, new_address):
        """ Configure new address for the light modbus device. Applied after device is reset

        :param new_address:
        """
        self._instrument.write_register(self._registers["Serial Address"], new_address)

    def get_address(self):
        """ Read device address register

        :return: address
        """
        return self._instrument.read_register(self._registers["Serial Address"])

    def set_baudrate(self, new_baudrate):
        """ Configure new baudrate for the light modbus device. Applied after device is reset

        :param new_baudrate: Must be a value in valid_baudrate. Otherwise function call is ignored
        """
        if new_baudrate in self.valid_baudrate:
            reg_value = self._baudrate_lookup[new_baudrate]
            self._instrument.write_register(self._registers["Serial Baud Rate"], reg_value)
        else:
            raise ValueError(f"Valid values are {self.valid_baudrate}")

    def get_baudrate(self):
        """ Read baudrate register

        :return: Baud rate value
        """
        register_value = self._instrument.read_register(self._registers["Serial Baud Rate"])
        key_index = list(self._baudrate_lookup.values()).index(register_value)

        return list(self._baudrate_lookup.keys())[key_index]

    def set_parity(self, new_parity):
        """ Configure new parity for the light modbus device. Applied after device is reset

        :param new_parity: 'E' (Even), 'O' (Odd) or 'N' (None)
        """
        if new_parity in self.valid_parity:
            reg_value = self._parity_lookup[new_parity]
            self._instrument.write_register(self._registers["Serial Parity"], reg_value)
        else:
            raise ValueError(f"Valid values are {self.valid_parity}")

    def get_parity(self):
        """ Read parity register

        :return: current parity. 'E' (Even), 'O' (Odd) or 'N' (None)
        """
        register_value = self._instrument.read_register(self._registers["Serial Parity"])
        key_index = list(self._parity_lookup.values()).index(register_value)

        return list(self._parity_lookup.keys())[key_index]

    def set_stop_bit(self, new_stop_bit):
        """ Configure new stop bit for the light modbus device. Applied after device is reset

        :param new_stop_bit:
        """
        if new_stop_bit in self.valid_stop_bit:
            self._instrument.write_register(self._registers["Serial Stop Bit"], new_stop_bit)
        else:
            raise ValueError(f"Valid values are {self.valid_stop_bit}")

    def get_stop_bit(self):
        """ Read stop bit register

        :return: stop bit value
        """
        stop_bit = self._instrument.read_register(self._registers["Serial Stop Bit"])
        return stop_bit

    def reset(self):
        """ Reset device.
        """
        self._instrument.write_register(self._registers["Reset"], self._reset_key)

    def get_firmware_version(self):
        """ Return device current firmware version

        :return: Firmware version 'x.y.z'
        """
        version = self._instrument.read_register(self._registers["FW version"])

        return str(((version & 0xF00) >> 8)) + '.' + str(((version & 0x0F0) >> 4)) + '.' + str((version & 0x00F))

    def is_device_responding(self):
        version = ''

        try:
            version = self.get_firmware_version()
        except Exception:
            pass

        if version == '':
            return False
        else:
            return True

    @staticmethod
    def _rtc_calibration_cal(desired_frequency_hz, measured_frequency_hz) -> int:
        calp = 0
        calp_pos = 15
        calm_max = 512

        if measured_frequency_hz < desired_frequency_hz:
            calp = 1

        # Calculate all possibilities to find CALM
        all_calibration_value = calp << calp_pos | np.arange(calm_max)
        calibrated_output = []

        for i in all_calibration_value:
            temp = LightModbus._calibrated_clock(measured_frequency_hz, i)
            calibrated_output.append(temp)

        # Find minimal error
        calibrated_output = np.array(calibrated_output)
        error = abs(1 - desired_frequency_hz / calibrated_output)
        index = np.where(error == min(error))[0][0]
        calibration_value = all_calibration_value[index]

        return calibration_value

    @staticmethod
    def _calibrated_clock(input_clock_hz, calibration_value) -> int:
        calp_pos = 15
        calp = (calibration_value & (1 << calp_pos)) >> calp_pos

        calm_mask = 0x1FF
        calm = calibration_value & calm_mask

        calibrated_clock = input_clock_hz * (1 + (calp * 512 - calm) / (2**20 + calm - calp * 512))

        return calibrated_clock

    def disable_rtc_calibration(self) -> None:
        disable = 0
        self._instrument.write_register(self._registers["RTC Calibration"], disable)

    def calibrate_rtc(self, measured_frequency_hz) -> [int, int]:
        """

        :param measured_frequency_hz: Measured frequency on RTC calibration test point
        :return: Expected frequency and calibration value
        """

        calibration_value = LightModbus._rtc_calibration_cal(self._rtc_calibration_freq, measured_frequency_hz)
        expected_freq = LightModbus._calibrated_clock(measured_frequency_hz, calibration_value)

        self._instrument.write_register(self._registers["RTC Calibration"], calibration_value)

        return [expected_freq, calibration_value]

    def set_rtc_calibration_value(self, value) -> None:
        self._instrument.write_register(self._registers["RTC Calibration"], value)

    def get_rtc_calibration_value(self) -> int:
        return self._instrument.read_register(self._registers["RTC Calibration"])

    def set_time(self, t) -> None:
        """ Set time of the device

        :param t: Expect a time.struct_time type object. Abort if it a different type
        """
        if type(t) != time.struct_time:
            raise ValueError("Argument is not of type 'time.struct_time'")

        self._instrument.write_register(self._registers["RTC Second"], t.tm_sec)
        self._instrument.write_register(self._registers["RTC Minute"], t.tm_min)
        self._instrument.write_register(self._registers["RTC Hour"], t.tm_hour)
        self._instrument.write_register(self._registers["RTC Day of week"], (t.tm_wday + 1))
        # tm_wday range [0, 6], Monday is 0. Light modbus use [1, 7], Monday is 1.

    def get_time(self) -> time.struct_time:
        s = self._instrument.read_register(self._registers["RTC Second"])
        m = self._instrument.read_register(self._registers["RTC Minute"])
        h = self._instrument.read_register(self._registers["RTC Hour"])
        wday = self._instrument.read_register(self._registers["RTC Day of week"]) - 1

        t = time.struct_time((0, 0, 0, h, m, s, wday, 0, 0))

        return t


if __name__ == "__main__":
    light = LightModbus('/dev/ttyUSB0')

    serial_modbus_config = light.find_device_serial_config([247])

    config = light.get_modbus_config()

    if serial_modbus_config['Address'] is None:
        print("Device configuration has not been found. Check if device is plugged and powered")
        quit()

    try:
        fw_version = light.get_firmware_version()
        print("Version is: " + fw_version)
        print("Current serial configuration is " + str(serial_modbus_config))
    except IOError:
        print("Failure")
